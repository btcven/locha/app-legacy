FROM ubuntu:16.04

# Install build dependencies (and vim + picocom for editing/debugging)
RUN apt-get -qq update \
    && apt-get install -y gcc git curl make libncurses-dev flex bison gperf python python-serial python-pip \
                          cmake ninja-build \
                          ccache \
                          picocom socat \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV ESP32_TOOLCHAIN_URL=https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
ENV ESP32_BINUTILS_URL=https://github.com/espressif/binutils-esp32ulp/releases/download/v2.28.51-esp32ulp-20180809/binutils-esp32ulp-linux64-2.28.51-esp32ulp-20180809.tar.gz
ENV ESP_IDF_SHA=a62cbfec9ab681bddf8f8e45d9949b1b1f67a2ec


# Get the ESP32 toolchain
ENV ESP_TCHAIN_BASEDIR /opt/local/espressif
RUN mkdir -p $ESP_TCHAIN_BASEDIR \
    && curl -sS -o $ESP_TCHAIN_BASEDIR/esp32-toolchain.tar.gz \
            $ESP32_TOOLCHAIN_URL \
    && tar -xzf $ESP_TCHAIN_BASEDIR/esp32-toolchain.tar.gz \
           -C $ESP_TCHAIN_BASEDIR/ \
    && rm $ESP_TCHAIN_BASEDIR/esp32-toolchain.tar.gz

RUN mkdir -p $ESP_TCHAIN_BASEDIR \
    && curl -L -sS -o $ESP_TCHAIN_BASEDIR/esp32ulp-toolchain.tar.gz \
             $ESP32_BINUTILS_URL \
    && tar -xzf $ESP_TCHAIN_BASEDIR/esp32ulp-toolchain.tar.gz \
           -C $ESP_TCHAIN_BASEDIR/ \
    && rm $ESP_TCHAIN_BASEDIR/esp32ulp-toolchain.tar.gz

# Setup IDF_PATH
RUN mkdir -p /esp \
    && cd /esp \ 
    && git clone --recursive https://github.com/espressif/esp-idf.git \
    && cd /esp/esp-idf \
    && git reset --hard $ESP_IDF_SHA
RUN pip install -r /esp/esp-idf/requirements.txt
ENV IDF_PATH /esp/esp-idf


# Add the toolchain binaries to PATH
ENV PATH $ESP_TCHAIN_BASEDIR/xtensa-esp32-elf/bin:$ESP_TCHAIN_BASEDIR/esp32ulp-elf-binutils/bin:$IDF_PATH/tools:$PATH

# This is the directory where our project will show up
RUN mkdir -p /esp/project
WORKDIR /esp/project
CMD ["/bin/bash"]

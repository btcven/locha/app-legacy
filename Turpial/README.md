# Turpial


## What's Turpial


## Installing

### Check previous requirements for compiling and install

#### Setting Up ESP-IDF

See setup guides for detailed instructions to set up the ESP-IDF:

* [Getting Started Guide for the stable ESP-IDF version](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/)

1.  Setup of Toolchain

    The Turpial project is developing in a Linux(Ubuntu)/MacOS enviroment.
    To compile with ESP-IDF you need to get the following packages.


    - Ubuntu

        ```
        sudo apt-get install gcc git wget make libncurses-dev flex bison gperf python python-serial

        ```

        Toolchain setup:
        ```
        mkdir -p ~/esp
        cd ~/esp
        wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
        tar -xzf xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz

        ```

        Setup the toolchain adding to `~/.profile` file:
        ```
        export PATH="$PATH:$HOME/esp/xtensa-esp32-elf/bin"
        ```

        


2.  Getting of ESP-IDF from GitHub

    - Ubuntu

        ```
        cd ~/esp
        git clone -b v3.1.2 --recursive https://github.com/espressif/esp-idf.git
        ```

        Setup the IDF-PATH adding to `~/.profile` file:
        ```
        export IDF_PATH=~/esp/esp-idf
        
        ```


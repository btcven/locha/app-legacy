//  (c) Copyright 2019 locha project developers
//  (c) Copyright 2019 @Jaz_B
//  (c) Copyright 2019 @rdymac
//  (c) Copyright 2019 @luisan00
//  License?

#include "ux.h"
#include "config.h"

SSD1306  display(OLED_ADD, OLED_SDA, OLED_SCL, OLED_RST);

infoScreen::infoScreen() {
  printf("Allocating display module");
}

infoScreen::~infoScreen() {
  // nothing
}

void infoScreen::init(){
  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);
  delay(50);
  display.init();
  display.flipScreenVertically();
  this->update();

}

void infoScreen::update() {
  display.clear();
  // Set Font & alingnment
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  // battery level
  if (bat >  0  && bat < 25) display.drawXbm(128 - BAT_width, 0, BAT_width, BAT_height, BAT_bits_0);
  if (bat >= 25 && bat < 50) display.drawXbm(128 - BAT_width, 0, BAT_width, BAT_height, BAT_bits_25);
  if (bat >= 50 && bat < 75) display.drawXbm(128 - BAT_width, 0, BAT_width, BAT_height, BAT_bits_50);
  if (bat >= 75 && bat < 90) display.drawXbm(128 - BAT_width, 0, BAT_width, BAT_height, BAT_bits_75);
  if (bat >= 90) display.drawXbm(128 - BAT_width, 0, BAT_width, BAT_height, BAT_bits_100);

  // show BT logo if BT is enabled & connected clients.
  if(ble_enabled) display.drawXbm(128 - BT_width, BT_height + 5, BT_width, BT_height, BT_bits);
  if(ble) display.drawString(128 - BT_width, BT_height + 5, String(ble));

  // show Turpial AccesPoint if enabled & connected clients.
  if(apt_enabled) display.drawXbm(128 - WIFI_width, 30, WIFI_width, WIFI_height, WIFI_bits);
  if(apt) display.drawString(128 - WIFI_width, 30, String(apt));

  display.drawString(128, 50, "ST " + String(sta));
  // Align center
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  // show screen title
  display.drawString(45, 0, "LoRa");
  // Set Font & alingnment
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  // show fields and values for LoRa RSSI & SNR values
  display.drawString(15, 20, "RSSI " + String(rssi));
  display.drawString(15, 40, "SNR  " + String(snr));
  display.display();
}

void infoScreen::setBAT(int n) {
  bat = n;
}

void infoScreen::setBLE(bool isEnabled, int n) {
  ble_enabled = isEnabled;
  ble = n;
}

void infoScreen::setAPT(bool isEnabled, int n) {
  apt_enabled = isEnabled;
  apt = n;
}

void infoScreen::setSTA(bool isEnabled, int n) {
  sta_enabled = isEnabled;
  sta = n;
}

void infoScreen::setRSSI(int n) {
  rssi = n;
}

void infoScreen::setSNR(int n) {
  snr = n;
}

void infoScreen::setContrast(int n) {
  contrast = n;
  display.setContrast(contrast);
}

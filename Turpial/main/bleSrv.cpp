#include <Arduino.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_log.h"

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include "config.h"

const char TAG[] = "ble.locha.io";

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;

// change the max length to send
std::string txVal;

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E" // RX
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" // TX

class serverCB: public BLEServerCallbacks {
	void onConnect(BLEServer* pServer) {
		deviceConnected = true;
	}

	void onDisconnect(BLEServer* pServer) {
		deviceConnected = false;
	}
};

class eventCB: public BLECharacteristicCallbacks {
	void onWrite(BLECharacteristic *pCharacteristic) {
		std::string rxValue = pCharacteristic->getValue();
		// parse messages here if the rxValue > 0 :
		//
		// for example:
		// - SET value of wifi AP ssid to "myssid"
		// 		SET_CONF, WIFI_AP, SSID, myssid (set ssid)
		//
		// - GET value of  wifi AP ssid:
		//		GET_CONF, WIFI_AP, SSID (return ssid)
		printf("$ ");
		if (rxValue.length() > 0) {
			for (int i = 0; i < rxValue.length(); i++) {
				printf("%c", rxValue[i]);
			}
			printf("\n");
		}
	}
};


bool startNVS(void){
	esp_err_t ret = nvs_flash_init();
	switch (ret) {
		case ESP_ERR_NVS_NO_FREE_PAGES:
			ESP_LOGI(TAG, "<NVS> ESP_ERR_NVS_NO_FREE_PAGES"); // @suppress("Invalid arguments")
			return false;
			break;
		case ESP_ERR_NOT_FOUND:
			ESP_LOGI(TAG, "<NVS> ESP_ERR_NOT_FOUND"); // @suppress("Invalid arguments")
			return false;
		default:
			ESP_LOGI(TAG, "<NVS> ESP_OK"); // @suppress("Invalid arguments")
			return true;
			break;
	}
}

void task_ble(void *pvParams) {
	// do something if false, :
	bool nvsStarted = startNVS();


	// Create the BLE DevicedeviceConnected
	BLEDevice::init(TAG);
	// Create the BLE Server
	pServer = BLEDevice::createServer();
	pServer->setCallbacks(new serverCB());

	// Create the BLE Service
	BLEService *pService = pServer->createService(SERVICE_UUID);

	// Create a BLE Characteristic
	pTxCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_TX,
			BLECharacteristic::PROPERTY_NOTIFY);

	pTxCharacteristic->addDescriptor(new BLE2902());

	BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
	CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);
	pRxCharacteristic->setCallbacks(new eventCB());

	// Start the service
	pService->start();
	// Start advertising
	pServer->getAdvertising()->start();

	while (1) {
		// set a value for txValue
		if (deviceConnected) {
			// set the value for txValue
			if (txVal.size() > 0) {
				pTxCharacteristic->setValue(txVal);
				pTxCharacteristic->notify();
			}
			// bluetooth will go into congestion if too many packets are sent
			// Increment the task's delay if has losses.
			vTaskDelay(10 / portTICK_PERIOD_MS);
		}
		// client disconnected
		if (!deviceConnected && oldDeviceConnected) {
			// give the bluetooth stack the chance to get ready again.
			vTaskDelay(500 / portTICK_PERIOD_MS);
			// restart advertising
			pServer->startAdvertising();
			ESP_LOGI(TAG, "start advertising"); // @suppress("Invalid arguments")
			oldDeviceConnected = deviceConnected;
		}
		// connecting
		if (deviceConnected && !oldDeviceConnected) {
			// do stuff here on connecting
			oldDeviceConnected = deviceConnected;
		}

	}
}


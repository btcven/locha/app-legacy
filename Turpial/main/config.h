#ifndef CONFIG_H_
#define CONFIG_H_

//  OLED Pin Map.
//
//   #  Adafruit  Heltec
//  SCL 22        15
//  SDA 23        4
//  RST 5         16
// #define HELTEC
//#define BOARD_ADAFRUIT
#define BOARD_HELTEC

// if oled adafruit
#ifdef  BOARD_ADAFRUIT
  #define OLED_SCL 22
  #define OLED_SDA 23
  #define OLED_RST 5
  #define OLED_ADD 0x3c
  //Heltec_v2 Vext control
  #define V2  false
  #ifdef V2
    #define Vext  21
  #endif

#endif

// if oled Heltec
#ifdef  BOARD_HELTEC
  #define OLED_SCL   15
  #define OLED_SDA   4
  #define OLED_RST   16 //set by software
  #define OLED_ADD 0x3c
  //Heltec_v2 Vext control
  #define V2  1
  #ifdef V2 //Heltec_v2 Vext control
    #define Vext  21
  #endif
#endif // oled_heltec

#define DEF_ENABLE_BLE		1
#define DEF_ENABLE_WIFI_AP	0
#define DEF_ENABLE_WIFI_STA	0
#define DEF_ENABLE_LORA		0


#endif

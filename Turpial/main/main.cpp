#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "bleSrv.h"

#include "CPPNVS.h"

#include "ux.h"
#include "config.h"

char TAG[] = "main";

extern "C" {
void app_main(void);
}
uint32_t bleEnabled;
uint32_t wapEnabled;


int deviceEnabled(std::string dev, int setDefault) {
	// start the nvs
	NVS mainNVS(dev);
	uint32_t isEnabled=0;
	esp_err_t keyExists = mainNVS.get("enabled", isEnabled);
	if (keyExists == ESP_OK) {
		ESP_LOGI(TAG, "%s : %d\n", dev.c_str(), isEnabled); // @suppress("Invalid arguments")
	} else {
		mainNVS.set("enabled", setDefault);
		mainNVS.commit();
	}
	mainNVS.~NVS();
	return isEnabled;
}

void app_main(void) {

<<<<<<< HEAD
  oledDisplay.init();
  wifi.setWifiEventHandler(apstaEvent);
  wifi.connectAP("","");
  wifi.startAP(apSSID, apPASS, apAuth, apChannel, ssidHidden, maxConnection);
  // delete next if delay functions are deleted.
  initArduino();
  // Create the BLE DevicedeviceConnected
  BLEDevice::init(TAG);

  // Create the BLE Server
  pServer = BLEDevice::createServer();

  pServer->setCallbacks(new serverCB());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  /*
  pTxCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);
  pTxCharacteristic->addDescriptor(new BLE2902());
  */
  pTxCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);
  pTxCharacteristic->setCallbacks(new eventCB());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);
  pRxCharacteristic->setCallbacks(new eventCB());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->addServiceUUID(pService->getUUID());
  pServer->getAdvertising()->start();
  //
  if (deviceConnected) {
    pTxCharacteristic->setValue(&txValue, 1);
    pTxCharacteristic->notify();
    txValue++;
    delay(10); // bluetooth stack will go into congestion, if too many packets are sent
  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
      delay(500); // give the bluetooth stack the chance to get things ready
      pServer->startAdvertising(); // restart advertising
      ESP_LOGI(TAG, "start advertising");
      oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
      // do stuff here on connecting
      oldDeviceConnected = deviceConnected;
  }
	// Enable BT stack on boot?
	if(deviceEnabled("ble", DEF_ENABLE_BLE) == 1) {
		xTaskCreate(task_ble, "task_ble", 2048, NULL, 5, NULL);
	} else {
		ESP_LOGI(TAG, "skip BT boot"); // @suppress("Invalid arguments")
	}

	// Enable Wifi AP mode on boot?
	if(deviceEnabled("wap", DEF_ENABLE_WIFI_AP) == 1) {
		// xtaskCreate(task_wap, "task_wap", 2048, NULL, 5, NULL);
	} else {
		ESP_LOGI(TAG, "skip WiFi AP boot"); // @suppress("Invalid arguments")
	}

	// Enable WiFi Station to wireless router on boot?
	if(deviceEnabled("wst", DEF_ENABLE_WIFI_STA) == 1) {
		// xtaskCreate(task_wap, "task_wap", 2048, NULL, 5, NULL);
	} else {
		ESP_LOGI(TAG, "skip WiFi STA boot"); // @suppress("Invalid arguments")
	}

	// Enable LoRa Module on boot?
	if(deviceEnabled("rad", DEF_ENABLE_LORA) == 1) {
		// xtaskCreate(task_wap, "task_wap", 2048, NULL, 5, NULL);
	} else {
		ESP_LOGI(TAG, "skip LoRa boot"); // @suppress("Invalid arguments")
	}
}
